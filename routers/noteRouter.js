const express = require("express");
const router = express.Router();

const {
  getNotes,
  addNote,
  getNoteById,
  putNoteById,
  checkNoteById,
  deleteNoteById,
} = require("../controllers/noteController");
const authMiddleware = require("../middlewares/authMiddleware");

router.get("/notes", authMiddleware, getNotes);
router.post("/notes", authMiddleware, addNote);
router.get("/notes/:id", authMiddleware, getNoteById);
router.put("/notes/:id", authMiddleware, putNoteById);
router.patch("/notes/:id", authMiddleware, checkNoteById);
router.delete("/notes/:id", authMiddleware, deleteNoteById);

module.exports = router;
