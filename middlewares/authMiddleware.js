const jwt = require("jsonwebtoken");

const ReqError = require("../models/error");

const { secret } = require("../config/auth");

module.exports = (req, res, next) => {
  const authHeader = req.headers["authorization"];

  if (!authHeader) {
    res.status(401).json(new ReqError({ message: "No 'authorization' header found" }));
  }

  const [, jwtToken] = authHeader.split(" ");

  try {
    req.user = jwt.verify(jwtToken, secret);
    next();
  } catch (err) {
    return res.status(401).json(new ReqError({ message: "Unvalid JWT" }));
  }
};
