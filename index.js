const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();

const { port } = require('./config/server');
const { localUri, cloudUri } = require('./config/database');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');

app.use(cors());
app.use(express.json());

app.use('/api', userRouter);
app.use('/api', authRouter);
app.use('/api', noteRouter);

const connect = async () => {
  try {
    await mongoose
      .connect(cloudUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      })
      .then(() => console.log('MongoDB Connected...'));

    app.listen(port, () => {
      console.log(`Server started on ${port} port`);
    });
  } catch (error) {
    console.log('The error was occurred during connection\n', error);
  }
};

connect();
