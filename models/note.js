const mongoose = require("mongoose");

const Schema = mongoose.Schema;

module.exports = mongoose.model(
  "note",
  new Schema(
    {
      userId: {
        type: String,
      },
      completed: {
        type: Boolean,
        default: false,
      },
      text: {
        type: String,
      },
      createdDate: {
        type: String,
        default: new Date(),
      },
    },
    { versionKey: false }
  )
);
