const mongoose = require("mongoose");

const Schema = mongoose.Schema;

module.exports = mongoose.model(
  "error",
  new Schema(
    {
      message: {
        type: String,
        required: true,
      },
    },
    { _id: false }
  )
);
