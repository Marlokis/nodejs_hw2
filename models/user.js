const mongoose = require('mongoose');

const Schema = mongoose.Schema;

module.exports = mongoose.model(
  'user',
  new Schema(
    {
      username: {
        required: true,
        type: String,
        unique: true,
      },
      createdDate: {
        type: String,
      },
    },
    { versionKey: false }
  )
);
