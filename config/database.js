const localDb = {
  port: 27017,
  host: 'localhost',
  databaseName: 'NODEJS_HW2',
};

const cloudDb = {
  dbName: 'KitsunFEHW2',
  userName: 'Admin',
  userPassword: '123456q',
};

module.exports = {
  localDb,
  cloudDb,
  localUri: `mongodb://${localDb.host}:${localDb.port}/${localDb.databaseName}`,
  cloudUri: `mongodb+srv://${cloudDb.userName}:${cloudDb.userPassword}@crudhw2cluster.avd1k.mongodb.net/${cloudDb.dbName}?retryWrites=true&w=majority`,
};
