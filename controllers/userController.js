const User = require("../models/user");
const Credential = require("../models/credentials");
const Note = require("../models/note");
const ReqError = require("../models/error");

module.exports.getProfile = (req, res) => {
  User.findById(req.user._id)
    .exec()
    .then((user) => {
      if (!user) {
        return res.status(400).json(new ReqError({ message: "User not found" }));
      }

      res.json({ user });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};

module.exports.deleteUser = (req, res) => {
  User.findByIdAndRemove(req.user._id)
    .exec()
    .then((user) => {
      if (!user) {
        return res.status(400).json(new ReqError({ message: "User not found" }));
      }
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });

  Credential.findByIdAndRemove(req.user._id)
    .exec()
    .then()
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });

  Note.deleteMany({ userId: req.user._id })
    .exec()
    .then(() => {
      res.json({ message: "Success" });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};

module.exports.changePassword = (req, res) => {
  const { oldPassword, newPassword } = req.body;

  Credential.findByIdAndUpdate(req.user._id, {
    $set: { password: newPassword },
  })
    .exec()
    .then((credential) => {
      if (!oldPassword) {
        return res.status(400).json(new ReqError({ message: "Old password not provided" }));
      }
      if (!newPassword) {
        return res.status(400).json(new ReqError({ message: "New password not provided" }));
      }
      if (oldPassword !== credential.password) {
        return res.status(400).json(new ReqError({ message: "Incorrect input old password" }));
      }

      res.json({ message: "Success" });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};
