const Note = require("../models/note");
const ReqError = require("../models/error");

module.exports.getNotes = (req, res) => {
  const id = req.user._id;

  Note.find({ userId: id })
    .exec()
    .then((notes) => {
      res.json({ notes });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};

module.exports.addNote = (req, res) => {
  const { text } = req.body;
  const userId = req.user._id;

  if (text == "") {
    return res.status(400).json(new ReqError({ message: "Field 'text' should not be empty" }));
  }
  if (!text) {
    return res.status(400).json(new ReqError({ message: "Field 'text' not provided" }));
  }

  const note = new Note({ userId, text });

  note
    .save()
    .then(() => {
      res.json({ message: "Success" });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};

module.exports.getNoteById = (req, res) => {
  Note.findById(req.params.id)
    .exec()
    .then((note) => {
      if (!note) {
        return res.status(400).json(new ReqError({ message: "Note not found with given id" }));
      }

      res.json({ note });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};

module.exports.putNoteById = (req, res) => {
  const { text } = req.body;

  if (text == "") {
    return res.status(400).json(new ReqError({ message: "Field 'text' should not be empty" }));
  }
  if (!text) {
    return res.status(400).json(new ReqError({ message: "Field 'text' not provided" }));
  }

  Note.findByIdAndUpdate(req.params.id, { $set: { text } })
    .exec()
    .then((note) => {
      if (!note) {
        return res.status(400).json(new ReqError({ message: "Note not found with given id" }));
      }
      res.json({ message: "Success" });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};

module.exports.checkNoteById = async (req, res) => {
  let completed;

  await Note.findById(req.params.id)
    .exec()
    .then((note) => {
      if (!note) {
        return res.status(400).json(new ReqError({ message: "Note not found with given id" }));
      }
      completed = note.completed;
    })
    .catch((err) => {
      return res.status(500).json(new ReqError({ message: err.message }));
    });

  Note.findByIdAndUpdate(req.params.id, { $set: { completed: !completed } })
    .exec()
    .then((note) => {
      if (!note) {
        return res.status(400).json(new ReqError({ message: "Note not found with given id" }));
      }

      res.json({ message: "Success" });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};

module.exports.deleteNoteById = (req, res) => {
  Note.findByIdAndRemove(req.params.id)
    .exec()
    .then((note) => {
      if (!note) {
        return res.status(400).json(new ReqError({ message: "Note not found with given id" }));
      }

      res.json({ message: "Success" });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};
