const jwt = require('jsonwebtoken');

const User = require('../models/user');
const Credential = require('../models/credentials');
const ReqError = require('../models/error');

const { secret } = require('../config/auth');

module.exports.register = (req, res) => {
  const { username, password } = req.body;

  if (!username) {
    return res
      .status(400)
      .json(new ReqError({ message: 'No username provided' }));
  }
  if (!password) {
    return res
      .status(400)
      .json(new ReqError({ message: 'No password provided' }));
  }

  const user = new User({ username, createdDate: new Date() });

  user
    .save()
    .then(() => {
      const credential = new Credential({ _id: user._id, username, password });

      credential
        .save()
        .then(() => {
          res.json({ message: 'Success' });
        })
        .catch((err) => {
          res.status(500).json(new ReqError({ message: err.message }));
        });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};

module.exports.login = (req, res) => {
  const { username, password } = req.body;

  if (!username) {
    return res
      .status(400)
      .json(new ReqError({ message: 'No username provided' }));
  }
  if (!password) {
    return res
      .status(400)
      .json(new ReqError({ message: 'No password provided' }));
  }

  Credential.findOne({ username, password })
    .exec()
    .then((user) => {
      if (!user) {
        return res.status(400).json(
          new ReqError({
            message: 'No user with such username and password found',
          })
        );
      }

      res.json({ jwt_token: jwt.sign(JSON.stringify(user), secret) });
    })
    .catch((err) => {
      res.status(500).json(new ReqError({ message: err.message }));
    });
};
